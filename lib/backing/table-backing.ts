import { createElement, Fragment, ReactElement } from "react";
import { Stuck, TableModel } from "../ranges";
import { CellRequestResult, createNode, Node, RangePosition } from "./node";

export interface CellOffset {
  top: number;

  left: number;
}

export interface CellSize {
  width: number;

  height: number;
}

export interface CellDimensions {
  col: number;

  row: number;

  offset: CellOffset;

  size: CellSize;

  empty: boolean;

  stuck?: Stuck;
}

function accumulate(diffs: number[]): number[] {
  const result: number[] = [];
  let acc: number = 0;
  for (const h of diffs) {
    acc += h;
    result.push(acc);
  }
  return result;
}

function findOffset(offsets: number[], offset: number): number {
  let h: number = offsets.length;
  let l: number = 0;
  if (offset < 0 || offset >= offsets[h - 1]) {
    throw new Error(`Offset ${offset} is out of bounds [0, ${offsets[h - 1]})`);
  }
  while (l + 1 < h) {
    const p: number = Math.floor((l + h) / 2);
    if (offsets[p] < offset) {
      l = p;
    } else if (offsets[p] > offset) {
      h = p;
    } else {
      return p + 1;
    }
  }
  if (offsets[l] > offset) {
    return l;
  } else {
    return l + 1;
  }
}

export interface CellRenderer {
  render(colSelected: boolean, rowSelected: boolean): ReactElement;
}

export class TableBacking {
  private readonly _model: TableModel;
  private readonly _rowOffsets: number[];
  private readonly _columnOffsets: number[];
  private readonly _rangeTree: Node;

  private constructor(model: TableModel, rangeTree: Node, colOffsets: number[], rowOffsets: number[]) {
    this._model = model;
    this._rowOffsets = rowOffsets;
    this._columnOffsets = colOffsets;
    this._rangeTree = rangeTree;
  }

  get width(): number {
    return this._columnOffsets[this._columnOffsets.length - 1];
  }

  get height(): number {
    return this._rowOffsets[this._rowOffsets.length - 1];
  }

  getColumnAtOffset(widthOffset: number): number {
    return findOffset(this._columnOffsets, widthOffset);
  }

  getRowAtOffset(heightOffset: number): number {
    return findOffset(this._rowOffsets, heightOffset);
  }

  private checkRange(col: number, row: number): void {
    if (row >= this._rowOffsets.length || row < 0) {
      throw new Error(`Row should be greater than 0 and less than ${this._rowOffsets.length}, was ${row}`);
    }
    if (col >= this._columnOffsets.length || col < 0) {
      throw new Error(`Row should be greater than 0 and less than ${this._columnOffsets.length}, was ${col}`);
    }
  }

  private calculateCellDimensions({ position, cell, dimensions, stuck }: CellRequestResult): CellDimensions {
    const { col, row }: RangePosition = position;
    const left: number = col === 0 ? 0 : this._columnOffsets[col - 1];
    const top: number = row === 0 ? 0 : this._rowOffsets[row - 1];
    const right: number = this._columnOffsets[col + dimensions.cols - 1];
    const bottom: number = this._rowOffsets[row + dimensions.rows - 1];

    return {
      col,
      row,
      offset: { left, top },
      size: {
        width: right - left,
        height: bottom - top
      },
      empty: !cell,
      stuck
    };
  }

  getCellDimensions(col: number, row: number): CellDimensions {
    this.checkRange(col, row);
    let result: CellRequestResult | null = null;

    this._rangeTree.findCellsInRange(
      col,
      col + 1,
      row,
      row + 1,
      { cols: this._columnOffsets.length, rows: this._rowOffsets.length },
      (r) =>
        r.position.col <= col &&
        r.position.col + r.dimensions.cols > col &&
        r.position.row <= row &&
        r.position.row + r.dimensions.rows > row
          ? !!(result = r)
          : false
    );

    if (!result) {
      throw new Error(`Unexpected state for col ${col}, row ${row}`);
    }

    return this.calculateCellDimensions(result);
  }

  getCellRenderer(col: number, row: number): CellRenderer {
    this.checkRange(col, row);
    let result: CellRequestResult | null = null;

    this._rangeTree.findCellsInRange(
      col,
      col + 1,
      row,
      row + 1,
      { cols: this._columnOffsets.length, rows: this._rowOffsets.length },
      (r) =>
        r.position.col <= col &&
        r.position.col + r.dimensions.cols > col &&
        r.position.row <= row &&
        r.position.row + r.dimensions.rows > row
          ? !!(result = r)
          : false
    );

    if (!result) {
      throw new Error(`Unexpected state for col ${col}, row ${row}`);
    }

    const { cell }: CellRequestResult = result;

    if (cell) {
      return cell;
    } else {
      return {
        render(): ReactElement {
          return createElement(Fragment);
        }
      };
    }
  }

  getCells(
    fromWidthOffset: number,
    toWidthOffset: number,
    fromHeightOffset: number,
    toHeightOffset: number
  ): CellDimensions[] {
    if (fromWidthOffset === toWidthOffset || fromHeightOffset === toHeightOffset) {
      return [];
    }

    const fromCol: number = this.getColumnAtOffset(fromWidthOffset);
    const toCol: number = this.getColumnAtOffset(toWidthOffset - 1) + 1;

    const fromRow: number = this.getRowAtOffset(fromHeightOffset);
    const toRow: number = this.getRowAtOffset(toHeightOffset - 1) + 1;

    const result: CellRequestResult[] = [];
    this._rangeTree.findCellsInRange(
      fromCol,
      toCol,
      fromRow,
      toRow,
      { cols: this._columnOffsets.length, rows: this._rowOffsets.length },
      (r) => !result.push(r) && false
    );

    return result.map((r) => this.calculateCellDimensions(r));
  }

  setModel(model: TableModel): TableBacking {
    if (model === this._model) {
      return this;
    }

    const colOffsets: number[] = accumulate(model.colWidths);
    const colMatches: boolean =
      colOffsets.length === this._columnOffsets.length &&
      colOffsets.reduce((r, o, i) => r && o === this._columnOffsets[i], true);

    const rowOffsets: number[] = accumulate(model.rowHeights);
    const rowMatches: boolean =
      rowOffsets.length === this._rowOffsets.length &&
      rowOffsets.reduce((r, o, i) => r && o === this._rowOffsets[i], true);

    const range: Node = this._rangeTree.setRange(model.content, { cols: colOffsets.length, rows: rowOffsets.length });

    if (range !== this._rangeTree || !colMatches || !rowMatches) {
      return new TableBacking(model, range, colOffsets, rowOffsets);
    } else {
      return this;
    }
  }

  static createBacking(model: TableModel): TableBacking {
    const colOffsets: number[] = accumulate(model.colWidths);
    const rowOffsets: number[] = accumulate(model.rowHeights);

    return new TableBacking(
      model,
      createNode(model.content, { cols: colOffsets.length, rows: rowOffsets.length }),
      colOffsets,
      rowOffsets
    );
  }
}
