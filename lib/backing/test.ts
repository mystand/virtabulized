/* tslint:disable:max-classes-per-file */
import { createElement, Fragment, ReactElement } from "react";
import { Cell, HorizontalRange, RangeContent, RangeDimensions, RangeKey, TableModel, VerticalRange } from "../ranges";
import { CellDimensions, TableBacking } from "./table-backing";

class SimpleCell implements Cell {
  readonly type: "cell" = "cell";
  readonly dimensions: RangeDimensions;

  constructor(readonly key: RangeKey, colspan: number = 1, rowspan: number = 1) {
    if (colspan < 0 || rowspan < 0 || !colspan || !rowspan) {
      throw new Error("Illegal rows and cols");
    }
    this.dimensions = {
      cols: colspan,
      rows: rowspan
    };
  }

  render(): ReactElement<void> {
    throw new Error("Not implemented");
  }
}

class SimpleVerticalRange implements VerticalRange {
  type: "vertical" = "vertical";
  sticky: boolean = false;

  constructor(
    readonly key: RangeKey,
    private readonly children: RangeContent[],
    readonly dimensions?: RangeDimensions
  ) {}

  getChildren(): RangeContent[] {
    return this.children;
  }
}

class SimpleHorizontalRange implements HorizontalRange {
  type: "horizontal" = "horizontal";
  sticky: boolean = false;

  constructor(
    readonly key: RangeKey,
    private readonly children: RangeContent[],
    readonly dimensions?: RangeDimensions
  ) {}

  getChildren(): RangeContent[] {
    return this.children;
  }
}

describe("TableBacking", () => {
  it("is truthy", () => expect(TableBacking).toBeTruthy());
  it("instantiates", () => {
    expect(
      TableBacking.createBacking({
        colWidths: [100],
        rowHeights: [100],
        content: new SimpleCell("a")
      })
    ).toBeTruthy();
  });
  it("has proper height", () => {
    expect(
      TableBacking.createBacking({
        colWidths: [100],
        rowHeights: [200],
        content: new SimpleCell("a")
      }).height
    ).toBe(200);
  });
  it("has proper width", () => {
    expect(
      TableBacking.createBacking({
        colWidths: [100],
        rowHeights: [200],
        content: new SimpleCell("a")
      }).width
    ).toBe(100);
  });
  it("has proper height, with multiple rows", () => {
    expect(
      TableBacking.createBacking({
        colWidths: [100],
        rowHeights: [200, 2000],
        content: new SimpleCell("a")
      }).height
    ).toBe(2200);
  });
  it("has proper width, with multiple columns", () => {
    expect(
      TableBacking.createBacking({
        colWidths: [100, 1000],
        rowHeights: [200],
        content: new SimpleCell("a")
      }).width
    ).toBe(1100);
  });
  it("finds correct column", () => {
    const tb: TableBacking = TableBacking.createBacking({
      colWidths: [100, 1000, 10000],
      rowHeights: [200, 2000, 20000],
      content: new SimpleCell("a")
    });
    expect(tb.getColumnAtOffset(0)).toBe(0);
    expect(tb.getColumnAtOffset(50)).toBe(0);
    expect(tb.getColumnAtOffset(100)).toBe(1);
    expect(tb.getColumnAtOffset(150)).toBe(1);
    expect(tb.getColumnAtOffset(1100)).toBe(2);
    expect(tb.getColumnAtOffset(1300)).toBe(2);
    expect(() => tb.getColumnAtOffset(11100)).toThrow();
    expect(() => tb.getColumnAtOffset(11111)).toThrow();
  });
  it("finds correct row", () => {
    const tb: TableBacking = TableBacking.createBacking({
      colWidths: [100, 1000, 10000],
      rowHeights: [200, 2000, 20000],
      content: new SimpleCell("a")
    });
    expect(tb.getRowAtOffset(0)).toBe(0);
    expect(tb.getRowAtOffset(100)).toBe(0);
    expect(tb.getRowAtOffset(200)).toBe(1);
    expect(tb.getRowAtOffset(300)).toBe(1);
    expect(tb.getRowAtOffset(2200)).toBe(2);
    expect(tb.getRowAtOffset(2500)).toBe(2);
    expect(() => tb.getRowAtOffset(22200)).toThrow();
    expect(() => tb.getRowAtOffset(22222)).toThrow();
  });
});

describe("TableBacking", () => {
  it("rejects incorrect inputs", () => {
    const tbl: TableBacking = TableBacking.createBacking({
      colWidths: [100],
      rowHeights: [200],
      content: new SimpleCell("a")
    });
    expect(() => tbl.getCellDimensions(-1, 0)).toThrow();
    expect(() => tbl.getCellDimensions(1, 0)).toThrow();
    expect(() => tbl.getCellDimensions(0, -1)).toThrow();
    expect(() => tbl.getCellDimensions(0, 1)).toThrow();
    expect(() => tbl.getCellDimensions(1, 1)).toThrow();
    expect(() => tbl.getCellDimensions(-1, -1)).toThrow();
  });
  it("has correct cell span", () => {
    const span: CellDimensions = TableBacking.createBacking({
      colWidths: [1, 2],
      rowHeights: [8, 16, 32, 64],
      content: new SimpleCell("a", 2, 4)
    }).getCellDimensions(0, 0);
    expect(span).toBeTruthy();
    expect(span.col).toBe(0);
    expect(span.row).toBe(0);
    expect(span.offset).toBeTruthy();
    expect(span.offset.left).toBe(0);
    expect(span.offset.top).toBe(0);
    expect(span.size.width).toBe(3);
    expect(span.size.height).toBe(8 + 16 + 32 + 64);
    expect(span.empty).toBeFalsy();
    expect(span.stuck).toBeFalsy();
  });
  it("has correct cell position", () => {
    const span: CellDimensions = TableBacking.createBacking({
      colWidths: [2, 4],
      rowHeights: [8, 16, 32, 64],
      content: new SimpleCell("a", 2, 4)
    }).getCellDimensions(1, 2);
    expect(span).toBeTruthy();
    expect(span.offset).toBeTruthy();
    expect(span.offset.left).toBe(0);
    expect(span.offset.top).toBe(0);
    expect(span.size).toBeTruthy();
    expect(span.size.width).toBe(2 + 4);
    expect(span.size.height).toBe(8 + 16 + 32 + 64);
    expect(span.empty).toBeFalsy();
    expect(span.stuck).toBeFalsy();
  });

  it("has correct right margin span", () => {
    const span: CellDimensions = TableBacking.createBacking({
      colWidths: [2, 4, 8, 16],
      rowHeights: [32, 64, 128, 256],
      content: new SimpleCell("a", 2, 2)
    }).getCellDimensions(2, 1);
    expect(span).toBeTruthy();
    expect(span.offset).toBeTruthy();
    expect(span.offset.left).toBe(2 + 4);
    expect(span.offset.top).toBe(0);
    expect(span.size).toBeTruthy();
    expect(span.size.width).toBe(8 + 16);
    expect(span.size.height).toBe(32 + 64);
    expect(span.empty).toBeTruthy();
    expect(span.stuck).toBeFalsy();
  });
  it("has correct bottom margin span", () => {
    const span: CellDimensions = TableBacking.createBacking({
      colWidths: [2, 4, 8, 16],
      rowHeights: [32, 64, 128, 256],
      content: new SimpleCell("a", 2, 2)
    }).getCellDimensions(1, 2);
    expect(span).toBeTruthy();
    expect(span.offset).toBeTruthy();
    expect(span.offset.left).toBe(0);
    expect(span.offset.top).toBe(32 + 64);
    expect(span.size).toBeTruthy();
    expect(span.size.width).toBe(2 + 4 + 8 + 16);
    expect(span.size.height).toBe(128 + 256);
    expect(span.empty).toBeTruthy();
    expect(span.stuck).toBeFalsy();
  });
});

// vertical ranges
describe("TableBacking", () => {
  it("has correct span of a first cell in a vertical range", () => {
    const span: CellDimensions = TableBacking.createBacking({
      colWidths: [2, 4, 8, 16],
      rowHeights: [32, 64, 128, 256],
      content: new SimpleVerticalRange("a", [new SimpleCell("a", 2, 2), new SimpleCell("b", 1, 1)])
    }).getCellDimensions(0, 0);
    expect(span).toBeTruthy();
    expect(span.offset).toBeTruthy();
    expect(span.offset.left).toBe(0);
    expect(span.offset.top).toBe(0);
    expect(span.size).toBeTruthy();
    expect(span.size.width).toBe(2 + 4);
    expect(span.size.height).toBe(32 + 64);
    expect(span.empty).toBeFalsy();
    expect(span.stuck).toBeFalsy();
  });
  it("has correct position of a first cell in a vertical range", () => {
    const span: CellDimensions = TableBacking.createBacking({
      colWidths: [2, 4, 8, 16],
      rowHeights: [32, 64, 128, 256],
      content: new SimpleVerticalRange("a", [new SimpleCell("a", 2, 2), new SimpleCell("b", 1, 1)])
    }).getCellDimensions(1, 1);
    expect(span).toBeTruthy();
    expect(span.offset).toBeTruthy();
    expect(span.offset.left).toBe(0);
    expect(span.offset.top).toBe(0);
    expect(span.size).toBeTruthy();
    expect(span.size.width).toBe(2 + 4);
    expect(span.size.height).toBe(32 + 64);
    expect(span.empty).toBeFalsy();
    expect(span.stuck).toBeFalsy();
  });
  it("has correct span of other cell in a vertical range", () => {
    const span: CellDimensions = TableBacking.createBacking({
      colWidths: [2, 4, 8, 16],
      rowHeights: [32, 64, 128, 256],
      content: new SimpleVerticalRange("a", [new SimpleCell("a", 2, 2), new SimpleCell("b", 1, 1)])
    }).getCellDimensions(0, 2);
    expect(span).toBeTruthy();
    expect(span.offset).toBeTruthy();
    expect(span.offset.left).toBe(0);
    expect(span.offset.top).toBe(32 + 64);
    expect(span.size).toBeTruthy();
    expect(span.size.width).toBe(2);
    expect(span.size.height).toBe(128);
    expect(span.empty).toBeFalsy();
    expect(span.stuck).toBeFalsy();
  });
  it("has correct margin span of a vertical range", () => {
    const span: CellDimensions = TableBacking.createBacking({
      colWidths: [2, 4, 8, 16],
      rowHeights: [32, 64, 128, 256],
      content: new SimpleVerticalRange("a", [new SimpleCell("a", 2, 2), new SimpleCell("b", 1, 1)], {
        rows: 4,
        cols: 4
      })
    }).getCellDimensions(0, 3);
    expect(span).toBeTruthy();
    expect(span.offset).toBeTruthy();
    expect(span.offset.left).toBe(0);
    expect(span.offset.top).toBe(32 + 64 + 128);
    expect(span.size).toBeTruthy();
    expect(span.size.width).toBe(2 + 4 + 8 + 16);
    expect(span.size.height).toBe(256);
    expect(span.empty).toBeTruthy();
    expect(span.stuck).toBeFalsy();
  });
  it("has correct margin span of a cell in a horizontal range", () => {
    const span: CellDimensions = TableBacking.createBacking({
      colWidths: [2, 4, 8, 16],
      rowHeights: [32, 64, 128, 256],
      content: new SimpleVerticalRange("a", [new SimpleCell("a", 2, 2), new SimpleCell("b", 1, 1)], {
        rows: 4,
        cols: 4
      })
    }).getCellDimensions(1, 2);
    expect(span).toBeTruthy();
    expect(span.offset).toBeTruthy();
    expect(span.offset.left).toBe(2);
    expect(span.offset.top).toBe(32 + 64);
    expect(span.size).toBeTruthy();
    expect(span.size.width).toBe(4 + 8 + 16);
    expect(span.size.height).toBe(128);
    expect(span.empty).toBeTruthy();
    expect(span.stuck).toBeFalsy();
  });
});

// horizontal ranges
describe("TableBacking", () => {
  it("has correct span of a first cell in a horizontal range", () => {
    const span: CellDimensions = TableBacking.createBacking({
      colWidths: [2, 4, 8, 16],
      rowHeights: [32, 64, 128, 256],
      content: new SimpleHorizontalRange("a", [new SimpleCell("a", 2, 2), new SimpleCell("b", 1, 1)])
    }).getCellDimensions(0, 0);
    expect(span).toBeTruthy();
    expect(span.offset).toBeTruthy();
    expect(span.offset.left).toBe(0);
    expect(span.offset.top).toBe(0);
    expect(span.size).toBeTruthy();
    expect(span.size.width).toBe(2 + 4);
    expect(span.size.height).toBe(32 + 64);
    expect(span.empty).toBeFalsy();
    expect(span.stuck).toBeFalsy();
  });
  it("has correct position of a first cell in a horizontal range", () => {
    const span: CellDimensions = TableBacking.createBacking({
      colWidths: [2, 4, 8, 16],
      rowHeights: [32, 64, 128, 256],
      content: new SimpleHorizontalRange("a", [new SimpleCell("a", 2, 2), new SimpleCell("b", 1, 1)])
    }).getCellDimensions(1, 1);
    expect(span).toBeTruthy();
    expect(span.offset).toBeTruthy();
    expect(span.offset.left).toBe(0);
    expect(span.offset.top).toBe(0);
    expect(span.size).toBeTruthy();
    expect(span.size.width).toBe(2 + 4);
    expect(span.size.height).toBe(32 + 64);
    expect(span.empty).toBeFalsy();
    expect(span.stuck).toBeFalsy();
  });
  it("has correct span of other cell in a horizontal range", () => {
    const span: CellDimensions = TableBacking.createBacking({
      colWidths: [2, 4, 8, 16],
      rowHeights: [32, 64, 128, 256],
      content: new SimpleHorizontalRange("a", [new SimpleCell("a", 2, 2), new SimpleCell("b", 1, 1)])
    }).getCellDimensions(2, 0);
    expect(span).toBeTruthy();
    expect(span.offset).toBeTruthy();
    expect(span.offset.left).toBe(2 + 4);
    expect(span.offset.top).toBe(0);
    expect(span.size).toBeTruthy();
    expect(span.size.width).toBe(8);
    expect(span.size.height).toBe(32);
    expect(span.empty).toBeFalsy();
    expect(span.stuck).toBeFalsy();
  });
  it("has correct margin span of a horizontal range", () => {
    const span: CellDimensions = TableBacking.createBacking({
      colWidths: [2, 4, 8, 16],
      rowHeights: [32, 64, 128, 256],
      content: new SimpleHorizontalRange("a", [new SimpleCell("a", 2, 2), new SimpleCell("b", 1, 1)], {
        rows: 4,
        cols: 4
      })
    }).getCellDimensions(3, 0);
    expect(span).toBeTruthy();
    expect(span.offset).toBeTruthy();
    expect(span.offset.left).toBe(2 + 4 + 8);
    expect(span.offset.top).toBe(0);
    expect(span.size).toBeTruthy();
    expect(span.size.width).toBe(16);
    expect(span.size.height).toBe(32 + 64 + 128 + 256);
    expect(span.empty).toBeTruthy();
    expect(span.stuck).toBeFalsy();
  });
  it("has correct margin span of a cell in a horizontal range", () => {
    const span: CellDimensions = TableBacking.createBacking({
      colWidths: [2, 4, 8, 16],
      rowHeights: [32, 64, 128, 256],
      content: new SimpleHorizontalRange("a", [new SimpleCell("a", 2, 2), new SimpleCell("b", 1, 1)], {
        rows: 4,
        cols: 4
      })
    }).getCellDimensions(2, 1);
    expect(span).toBeTruthy();
    expect(span.offset).toBeTruthy();
    expect(span.offset.left).toBe(2 + 4);
    expect(span.offset.top).toBe(32);
    expect(span.size).toBeTruthy();
    expect(span.size.width).toBe(8);
    expect(span.size.height).toBe(64 + 128 + 256);
    expect(span.empty).toBeTruthy();
    expect(span.stuck).toBeFalsy();
  });
});

describe("TableBacking", () => {
  it("Renders a cell", () => {
    const cell: SimpleCell = new SimpleCell("a");
    cell.render = jest.fn((): ReactElement => createElement(Fragment));
    const tbl: TableBacking = TableBacking.createBacking({
      colWidths: [100, 1000],
      rowHeights: [200],
      content: cell
    });
    tbl.getCellRenderer(0, 0).render(false, false);
    expect(cell.render).toBeCalledTimes(1);
  });

  it("Renders a required cell", () => {
    const firstCell: SimpleCell = new SimpleCell("a");
    const secondCell: SimpleCell = new SimpleCell("b");
    firstCell.render = jest.fn((): ReactElement => createElement(Fragment));
    secondCell.render = jest.fn((): ReactElement => createElement(Fragment));
    const tbl: TableBacking = TableBacking.createBacking({
      colWidths: [100, 1000],
      rowHeights: [200],
      content: new SimpleHorizontalRange("c", [firstCell, secondCell])
    });
    tbl.getCellRenderer(0, 0).render(false, false);
    expect(firstCell.render).toBeCalledTimes(1);
    expect(secondCell.render).not.toBeCalled();
  });

  it("Renders a nested cell", () => {
    const targetCell: SimpleCell = new SimpleCell("a");
    const otherCells: SimpleCell[] = [null, null, null].map(
      (_: null, i: number): SimpleCell => {
        const cell: SimpleCell = new SimpleCell(`b${i}`);
        cell.render = jest.fn((): ReactElement => createElement(Fragment));
        return cell;
      }
    );
    targetCell.render = jest.fn((): ReactElement => createElement(Fragment));
    const tbl: TableBacking = TableBacking.createBacking({
      colWidths: [1, 2, 4, 8],
      rowHeights: [16, 32, 64],
      content: new SimpleVerticalRange("c", [
        new SimpleHorizontalRange("d", otherCells),
        new SimpleHorizontalRange("e", [...otherCells, targetCell]),
        new SimpleHorizontalRange("f", [otherCells[0], otherCells[1]])
      ])
    });
    tbl.getCellRenderer(3, 1).render(false, false);
    expect(targetCell.render).toBeCalledTimes(1);
    otherCells.forEach((cell) => expect(cell.render).not.toBeCalled());
  });
});

describe("TableBacking", () => {
  it("Fetches a cell", () => {
    const tbl: TableBacking = TableBacking.createBacking({
      colWidths: [100],
      rowHeights: [200],
      content: new SimpleCell("a")
    });
    const cells: CellDimensions[] = tbl.getCells(0, 100, 0, 200);
    expect(cells.length).toBe(1);
    const [cell]: CellDimensions[] = cells;
    expect(cell).toEqual({
      col: 0,
      row: 0,
      offset: {
        left: 0,
        top: 0
      },
      size: {
        width: 100,
        height: 200
      },
      empty: false,
    });
  });

  it("Fetches a cell with a padding", () => {
    const tbl: TableBacking = TableBacking.createBacking({
      colWidths: [1, 10],
      rowHeights: [2, 20],
      content: new SimpleCell("a")
    });
    const cells: CellDimensions[] = tbl.getCells(0, 11, 0, 22);
    expect(cells.length).toBe(3);
    expect(cells.find((c) => c.row === 0 && c.col === 0)).toEqual({
      col: 0,
      row: 0,
      offset: {
        left: 0,
        top: 0
      },
      size: {
        width: 1,
        height: 2
      },
      empty: false,
    });
    expect(cells.find((c) => c.row === 0 && c.col === 1)).toEqual({
      col: 1,
      row: 0,
      offset: {
        left: 1,
        top: 0
      },
      size: {
        width: 10,
        height: 2
      },
      empty: true
    });
    expect(cells.find((c) => c.row === 1 && c.col === 0)).toEqual({
      col: 0,
      row: 1,
      offset: {
        left: 0,
        top: 2
      },
      size: {
        width: 11,
        height: 20
      },
      empty: true
    });
  });

  it("Fetches a cell in a range with a padding", () => {
    const tbl: TableBacking = TableBacking.createBacking({
      colWidths: [1, 10, 100],
      rowHeights: [2, 20, 200],
      content: new SimpleHorizontalRange("a", [new SimpleCell("a")], { cols: 2, rows: 2 })
    });
    const cells: CellDimensions[] = tbl.getCells(0, 100, 0, 200);
    expect(cells.length).toBe(4);
    expect(cells.find((c) => c.row === 0 && c.col === 0)).toEqual({
      col: 0,
      row: 0,
      offset: {
        left: 0,
        top: 0
      },
      size: {
        width: 1,
        height: 2
      },
      empty: false
    });
    expect(cells.find((c) => c.row === 0 && c.col === 1)).toEqual({
      col: 1,
      row: 0,
      offset: {
        left: 1,
        top: 0
      },
      size: {
        width: 110,
        height: 22
      },
      empty: true
    });
    expect(cells.find((c) => c.row === 1 && c.col === 0)).toEqual({
      col: 0,
      row: 1,
      offset: {
        left: 0,
        top: 2
      },
      size: {
        width: 1,
        height: 20
      },
      empty: true
    });
    expect(cells.find((c) => c.row === 2 && c.col === 0)).toEqual({
      col: 0,
      row: 2,
      offset: {
        left: 0,
        top: 22
      },
      size: {
        width: 111,
        height: 200
      },
      empty: true
    });
  });

  it("Fetches a partially addressed cell", () => {
    const tbl: TableBacking = TableBacking.createBacking({
      colWidths: [1, 10, 100],
      rowHeights: [2, 20, 200],
      content: new SimpleVerticalRange("a", [
        new SimpleHorizontalRange("a", [new SimpleCell("a"), new SimpleCell("b")]),
        new SimpleHorizontalRange("b", [new SimpleCell("a"), new SimpleCell("b")])
      ])
    });
    const cells: CellDimensions[] = tbl.getCells(0, 9, 0, 19);
    expect(cells.length).toBe(4);
    expect(cells.find((c) => c.row === 0 && c.col === 0)).toEqual({
      col: 0,
      row: 0,
      offset: {
        left: 0,
        top: 0
      },
      size: {
        width: 1,
        height: 2
      },
      empty: false
    });
    expect(cells.find((c) => c.row === 0 && c.col === 1)).toEqual({
      col: 1,
      row: 0,
      offset: {
        left: 1,
        top: 0
      },
      size: {
        width: 10,
        height: 2
      },
      empty: false
    });
    expect(cells.find((c) => c.row === 1 && c.col === 0)).toEqual({
      col: 0,
      row: 1,
      offset: {
        left: 0,
        top: 2
      },
      size: {
        width: 1,
        height: 20
      },
      empty: false
    });
    expect(cells.find((c) => c.row === 1 && c.col === 1)).toEqual({
      col: 1,
      row: 1,
      offset: {
        left: 1,
        top: 2
      },
      size: {
        width: 10,
        height: 20
      },
      empty: false
    });
  });

  it("Fetches a partially addressed cell", () => {
    const tbl: TableBacking = TableBacking.createBacking({
      colWidths: [1, 10, 100],
      rowHeights: [2, 20, 200],
      content: new SimpleVerticalRange("a", [
        new SimpleHorizontalRange("a", [new SimpleCell("a"), new SimpleCell("b"), new SimpleCell("c")]),
        new SimpleHorizontalRange("b", [new SimpleCell("a"), new SimpleCell("b"), new SimpleCell("c")]),
        new SimpleHorizontalRange("c", [new SimpleCell("a"), new SimpleCell("b"), new SimpleCell("c")])
      ])
    });
    const cells: CellDimensions[] = tbl.getCells(0, 9, 0, 19);
    expect(cells.length).toBe(4);
    expect(cells.find((c) => c.row === 0 && c.col === 0)).toEqual({
      col: 0,
      row: 0,
      offset: {
        left: 0,
        top: 0
      },
      size: {
        width: 1,
        height: 2
      },
      empty: false
    });
    expect(cells.find((c) => c.row === 0 && c.col === 1)).toEqual({
      col: 1,
      row: 0,
      offset: {
        left: 1,
        top: 0
      },
      size: {
        width: 10,
        height: 2
      },
      empty: false
    });
    expect(cells.find((c) => c.row === 1 && c.col === 0)).toEqual({
      col: 0,
      row: 1,
      offset: {
        left: 0,
        top: 2
      },
      size: {
        width: 1,
        height: 20
      },
      empty: false
    });
    expect(cells.find((c) => c.row === 1 && c.col === 1)).toEqual({
      col: 1,
      row: 1,
      offset: {
        left: 1,
        top: 2
      },
      size: {
        width: 10,
        height: 20
      },
      empty: false
    });
  });
});

describe("TableBacking", () => {
  it("Does not change when same model is set", () => {
    const model: TableModel = {
      colWidths: [100, 1000],
      rowHeights: [200],
      content: new SimpleCell("a")
    };
    const tbl: TableBacking = TableBacking.createBacking(model);
    expect(tbl.setModel(model)).toBe(tbl);
  });

  it("Does not change when equal model is set", () => {
    const model: TableModel = {
      colWidths: [100, 1000],
      rowHeights: [200],
      content: new SimpleCell("a")
    };
    const tbl: TableBacking = TableBacking.createBacking({ ...model });
    expect(tbl.setModel(model)).toBe(tbl);
  });

  it("Does not change when comparable model is set", () => {
    const model: TableModel = {
      colWidths: [100, 1000],
      rowHeights: [200],
      content: new SimpleCell("a")
    };
    const tbl: TableBacking = TableBacking.createBacking({
      colWidths: [100, 1000],
      rowHeights: [200],
      content: model.content
    });
    expect(tbl.setModel(model)).toBe(tbl);
  });

  it("Does change when columns changes", () => {
    const model: TableModel = {
      colWidths: [100, 1000],
      rowHeights: [200],
      content: new SimpleCell("a")
    };
    const tbl: TableBacking = TableBacking.createBacking({
      ...model,
      colWidths: [101, 1000]
    });
    expect(tbl.setModel(model)).not.toBe(tbl);
  });

  it("Does change when row changes", () => {
    const model: TableModel = {
      colWidths: [100, 1000],
      rowHeights: [200],
      content: new SimpleCell("a")
    };
    const tbl: TableBacking = TableBacking.createBacking({
      ...model,
      rowHeights: [201]
    });
    expect(tbl.setModel(model)).not.toBe(tbl);
  });

  it("Does change when model changes", () => {
    const model: TableModel = {
      colWidths: [100, 1000],
      rowHeights: [200],
      content: new SimpleCell("a")
    };
    const tbl: TableBacking = TableBacking.createBacking({
      ...model,
      content: new SimpleCell("a")
    });
    expect(tbl.setModel(model)).not.toBe(tbl);
  });

  it("Does change when model order changes", () => {
    const cellA: SimpleCell = new SimpleCell("a");
    cellA.render = jest.fn((): ReactElement => createElement(Fragment));
    const cellB: SimpleCell = new SimpleCell("b");
    cellB.render = jest.fn((): ReactElement => createElement(Fragment));
    const model: TableModel = {
      colWidths: [100, 1000],
      rowHeights: [200],
      content: new SimpleHorizontalRange("c", [cellA, cellB])
    };
    const tbl: TableBacking = TableBacking.createBacking({
      ...model,
      content: new SimpleHorizontalRange("c", [cellB, cellA])
    });
    tbl.getCellRenderer(0, 0).render(false, false);
    expect(cellA.render).toBeCalledTimes(0);
    expect(cellB.render).toBeCalledTimes(1);
    const updated: TableBacking = tbl.setModel(model);
    expect(updated).not.toBe(tbl);
    updated.getCellRenderer(0, 0).render(false, false);
    expect(cellA.render).toBeCalledTimes(1);
    expect(cellB.render).toBeCalledTimes(1);
  });

  it("Does change when model order and type changes", () => {
    const cellA: SimpleCell = new SimpleCell("a");
    cellA.render = jest.fn((): ReactElement => createElement(Fragment));
    const cellB: SimpleCell = new SimpleCell("b");
    cellB.render = jest.fn((): ReactElement => createElement(Fragment));
    const model: TableModel = {
      colWidths: [100, 1000],
      rowHeights: [200],
      content: new SimpleHorizontalRange("c", [cellA, new SimpleHorizontalRange("b", [cellB])])
    };
    const tbl: TableBacking = TableBacking.createBacking({
      ...model,
      content: new SimpleHorizontalRange("c", [new SimpleHorizontalRange("a", [cellA]), cellB])
    });
    tbl.getCellRenderer(0, 0).render(false, false);
    expect(cellA.render).toBeCalledTimes(1);
    expect(cellB.render).toBeCalledTimes(0);
    const updated: TableBacking = tbl.setModel(model);
    expect(updated).not.toBe(tbl);
    updated.getCellRenderer(0, 0).render(false, false);
    expect(cellA.render).toBeCalledTimes(2);
    expect(cellB.render).toBeCalledTimes(0);
  });

  it("Keeps nested items", () => {
    const cell: SimpleCell = new SimpleCell("a");
    cell.render = jest.fn((): ReactElement => createElement(Fragment));
    const rangeContent: SimpleCell[] = [cell];
    const victimRange: VerticalRange = new SimpleVerticalRange("a", rangeContent);
    const model: TableModel = {
      colWidths: [100, 1000],
      rowHeights: [200],
      content: new SimpleHorizontalRange("a", [new SimpleCell("b"), victimRange])
    };
    const tbl: TableBacking = TableBacking.createBacking({
      ...model,
      content: new SimpleHorizontalRange("a", [new SimpleCell("b"), victimRange])
    });
    tbl.getCellRenderer(1, 0).render(false, false);
    expect(cell.render).toBeCalledTimes(1);
    rangeContent.shift(); // Quite dirtily mutating range content, expecting memoization to kick in and hide this change
    const newTable: TableBacking = tbl.setModel(model);
    expect(newTable).not.toBe(tbl);
    newTable.getCellRenderer(1, 0).render(false, false);
    expect(cell.render).toBeCalledTimes(2);
  });
});

describe("TableBacking", () => {
  it("Complains about key duplicates", () => {
    const tbl: TableBacking = TableBacking.createBacking({
      colWidths: [100, 1000],
      rowHeights: [200],
      content: new SimpleHorizontalRange("a", [new SimpleCell("b"), new SimpleCell("b")])
    });
    expect(() => tbl.getCellDimensions(0, 0)).toThrow();
  });
  it("Complains about missing keys", () => {
    const tbl: TableBacking = TableBacking.createBacking({
      colWidths: [100, 1000],
      rowHeights: [200],
      content: new SimpleHorizontalRange("a", [new SimpleCell((undefined as any) as string)])
    });
    expect(() => tbl.getCellDimensions(0, 0)).toThrow();
  });
  it("Complains about overflowing dimensions", () => {
    const tbl: TableBacking = TableBacking.createBacking({
      colWidths: [100, 1000],
      rowHeights: [200],
      content: new SimpleHorizontalRange("a", [new SimpleCell("a", 2, 1)], { cols: 1, rows: 1 })
    });
    expect(() => tbl.getCellDimensions(0, 0)).toThrow();
  });
  it("Complains about overflowing dimensions of a range", () => {
    const tbl: TableBacking = TableBacking.createBacking({
      colWidths: [100, 1000],
      rowHeights: [200],
      content: new SimpleHorizontalRange("a", [new SimpleVerticalRange("a", [], { cols: 2, rows: 1 })], {
        cols: 1,
        rows: 1
      })
    });
    expect(() => tbl.getCellDimensions(0, 0)).toThrow();
  });
  it("Complains about overflowing cumulative in a horizontal range", () => {
    const tbl: TableBacking = TableBacking.createBacking({
      colWidths: [100, 1000, 10000],
      rowHeights: [200],
      content: new SimpleHorizontalRange("a", [new SimpleCell("a", 2, 1), new SimpleCell("b", 2, 1)], {
        cols: 3,
        rows: 1
      })
    });
    expect(() => tbl.getCellDimensions(0, 0)).toThrow();
  });
  it("Complains about overflowing cumulative in a vertical range", () => {
    const tbl: TableBacking = TableBacking.createBacking({
      colWidths: [100],
      rowHeights: [200, 2000, 20000],
      content: new SimpleVerticalRange("a", [new SimpleCell("a", 1, 2), new SimpleCell("b", 1, 2)], {
        cols: 1,
        rows: 3
      })
    });
    expect(() => tbl.getCellDimensions(0, 0)).toThrow();
  });

  it("Complains about overflowing new nodes with old cells", () => {
    const cell: Cell = new SimpleCell("a", 2, 1);
    const model: TableModel = {
      colWidths: [100, 1000],
      rowHeights: [200],
      content: new SimpleHorizontalRange("a", [cell], { cols: 2, rows: 1 })
    };
    const tbl: TableBacking = TableBacking.createBacking(model);
    tbl.getCellDimensions(0, 0);
    expect(() =>
      tbl
        .setModel({
          ...model,
          content: new SimpleHorizontalRange("a", [cell], { cols: 1, rows: 1 })
        })
        .getCellDimensions(0, 0)
    ).toThrow();
  });
  it("Complains about overflowing new nodes with old ranges", () => {
    const range: SimpleVerticalRange = new SimpleVerticalRange("a", [], { cols: 2, rows: 1 });
    const model: TableModel = {
      colWidths: [100, 1000],
      rowHeights: [200],
      content: new SimpleHorizontalRange("a", [range], { cols: 2, rows: 1 })
    };
    const tbl: TableBacking = TableBacking.createBacking(model);
    tbl.getCellDimensions(0, 0);
    expect(() =>
      tbl
        .setModel({
          ...model,
          content: new SimpleHorizontalRange("a", [range], { cols: 1, rows: 1 })
        })
        .getCellDimensions(0, 0)
    ).toThrow();
  });
});
