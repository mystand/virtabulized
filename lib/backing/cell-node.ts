import { Cell, GroupContent, RangeDimensions, Stuck } from "../ranges";
import { assertDimensionsFit, CellRequestResult, Node, pad } from "./node";
import { RangeNode } from "./range-node";

export class CellNode implements Node {
  readonly _cell: Cell;
  readonly _dimensions: RangeDimensions;

  constructor(cell: Cell, enclosingDimensions?: RangeDimensions) {
    this._cell = cell;
    this._dimensions = cell.dimensions;

    if (enclosingDimensions) {
      assertDimensionsFit(this._dimensions, enclosingDimensions, this._cell.key);
    }
  }

  get dimensions(): RangeDimensions {
    return this._dimensions;
  }

  get stuck(): Stuck | undefined {
    return this._cell.stuck;
  }

  findCellsInRange(
    fromCol: number,
    toCol: number,
    fromRow: number,
    toRow: number,
    enclosingDimensions: RangeDimensions,
    consumer: (r: CellRequestResult) => boolean,
    stuck?: Stuck
  ): boolean {
    if (this._dimensions.cols > fromCol && this._dimensions.rows > fromRow) {
      const stop: boolean = consumer({
        cell: this._cell,
        stuck,
        dimensions: this._dimensions,
        position: { col: 0, row: 0 }
      });
      if (stop) {
        return stop;
      }
    }

    return pad(toCol, toRow, this._dimensions, enclosingDimensions, consumer);
  }

  setRange(range: GroupContent, enclosingDimensions?: RangeDimensions): Node {
    if (range === this._cell) {
      if (enclosingDimensions) {
        assertDimensionsFit(this._dimensions, enclosingDimensions, this._cell.key);
      }
      return this;
    }

    if (range.type === "cell") {
      return new CellNode(range, enclosingDimensions);
    } else {
      return new RangeNode(range, {}, enclosingDimensions);
    }
  }
}
