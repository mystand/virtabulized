import { GroupContent, RangeDimensions, RangeKey, Stuck } from "../ranges";
import { CellNode } from "./cell-node";
import { assertDimensionsFit, CellRequestResult, Node, pad } from "./node";

interface CellRangeCache {
  [key: string]: Node;
  [key: number]: Node;
}

const zeroDimensions: RangeDimensions = { cols: 0, rows: 0 };

function offsetConsumer(
  colOffset: number,
  rowOffset: number,
  consumer: (r: CellRequestResult) => boolean
): (r: CellRequestResult) => boolean {
  return (result: CellRequestResult): boolean =>
    consumer({
      ...result,
      position: {
        col: colOffset + result.position.col,
        row: rowOffset + result.position.row
      }
    });
}

export class RangeNode implements Node {
  private readonly _range: GroupContent;
  private _dimensions?: RangeDimensions;
  private _order: RangeKey[] | undefined;
  private _cache: CellRangeCache;

  constructor(range: GroupContent, cache: CellRangeCache, dimensionConstraints?: RangeDimensions) {
    this._range = range;
    this._dimensions = range.dimensions;
    this._cache = cache;
    if (this._dimensions && dimensionConstraints) {
      assertDimensionsFit(this._dimensions, dimensionConstraints, this._range.key);
    }
  }

  setRange(range: GroupContent, enclosingDimensions?: RangeDimensions): Node {
    if (this._range !== range) {
      if (range.type === "cell") {
        return new CellNode(range, enclosingDimensions);
      } else {
        return new RangeNode(range, this._cache, enclosingDimensions);
      }
    }

    if (this._dimensions && enclosingDimensions) {
      assertDimensionsFit(this._dimensions, enclosingDimensions, this._range.key);
    }

    return this;
  }

  get dimensions(): RangeDimensions {
    if (!this._dimensions) {
      this._dimensions = this.calculateDimensions();
    }

    return this._dimensions;
  }

  get stuck(): Stuck | undefined {
    return this._range.stuck;
  }

  findCellsInRange(
    fromCol: number,
    toCol: number,
    fromRow: number,
    toRow: number,
    enclosingDimensions: RangeDimensions,
    consumer: (r: CellRequestResult) => boolean,
    stuck?: Stuck
  ): boolean {
    if (!this._order) {
      this.fillCache();
    }

    if (!this._order) {
      throw new Error(`Unexpected state: order not set despite filling caches for ${this._range.key}`);
    }

    let offset: number = 0;
    switch (this._range.type) {
      case "horizontal":
        for (const key of this._order) {
          if (offset >= toCol) {
            break;
          }

          const node: Node = this._cache[key];
          const currentStuck: Stuck | undefined = node.stuck;
          if (currentStuck !== undefined || fromCol - offset < node.dimensions.cols) {
            const stop: boolean = node.findCellsInRange(
              currentStuck !== undefined ? 0 : fromCol - offset,
              Math.min(toCol - offset, node.dimensions.cols),
              fromRow,
              Math.min(toRow, this.dimensions.rows),
              { cols: node.dimensions.cols, rows: this.dimensions.rows },
              offsetConsumer(offset, 0, consumer),
              stuck || currentStuck
            );
            if (stop) {
              return stop;
            }
          }
          offset += node.dimensions.cols;
        }

        if (offset < toCol) {
          return pad(toCol, toRow, { cols: offset, rows: this.dimensions.rows }, enclosingDimensions, consumer);
        }

        return false;
      case "vertical":
      case "cellGroup":
        for (const key of this._order) {
          if (offset >= toRow) {
            break;
          }

          const node: Node = this._cache[key];
          const currentStuck: Stuck | undefined = node.stuck;
          if (currentStuck !== undefined || fromRow - offset < node.dimensions.rows) {
            const stop: boolean = node.findCellsInRange(
              fromCol,
              Math.min(toCol, this.dimensions.cols),
              currentStuck !== undefined ? 0 : fromRow - offset,
              Math.min(toRow - offset, node.dimensions.rows),
              { cols: this.dimensions.cols, rows: node.dimensions.rows },
              offsetConsumer(0, offset, consumer),
              stuck || currentStuck
            );

            if (stop) {
              return stop;
            }
          }

          offset += node.dimensions.rows;
        }

        if (offset < toRow) {
          return pad(toCol, toRow, { cols: this.dimensions.cols, rows: offset }, enclosingDimensions, consumer);
        }

        return false;
      default:
        throw new Error(`Unexpected range type ${this._range.type}`);
    }
  }

  private calculateDimensions(): RangeDimensions {
    if (!this._order) {
      this.fillCache();
    }

    if (!this._order) {
      throw new Error(`Unexpected state: order not set despite filling caches for ${this._range.key}`);
    }

    let reducer: (acc: RangeDimensions, node: RangeDimensions) => RangeDimensions;
    switch (this._range.type) {
      case "horizontal":
        reducer = (a: RangeDimensions, i: RangeDimensions): RangeDimensions => ({
          cols: a.cols + i.cols,
          rows: Math.max(a.rows, i.rows)
        });
        break;
      case "vertical":
      case "cellGroup":
        reducer = (a: RangeDimensions, i: RangeDimensions): RangeDimensions => ({
          cols: Math.max(a.cols, i.cols),
          rows: a.rows + i.rows
        });
        break;
      default:
        throw new Error(`Unexpected range type ${this._range.type}`);
    }

    return this._order
      .map((k: RangeKey): Node => this._cache[k])
      .map((n: Node): RangeDimensions => n.dimensions)
      .reduce(reducer, zeroDimensions);
  }

  private fillCache(): void {
    let children: GroupContent[];
    switch (this._range.type) {
      case "horizontal":
      case "vertical":
      case "cellGroup":
        children = this._range.getChildren();
        break;
      default:
        throw new Error(`Unexpected range type ${this._range.type}`);
    }

    const order: RangeKey[] = [];
    const cache: CellRangeCache = {};
    let constraint: RangeDimensions | undefined = this._dimensions;

    for (const childRange of children) {
      const key: RangeKey = childRange.key;
      if (key === null || key === undefined) {
        throw new Error(`Undefined key in range with ${this._range.key}`);
      }
      order.push(key);
      const oldOne: Node | undefined = this._cache ? this._cache[key] : undefined;
      if (key in cache) {
        throw new Error(`Duplicate key: ${key}`);
      }
      let childNode: Node;
      if (oldOne) {
        childNode = oldOne.setRange(childRange, constraint);
      } else if (childRange.type === "cell") {
        childNode = new CellNode(childRange, constraint);
      } else {
        childNode = new RangeNode(childRange, {}, constraint);
      }
      cache[key] = childNode;
      if (constraint && ((childNode instanceof RangeNode && childNode._dimensions) || childNode instanceof CellNode)) {
        switch (this._range.type) {
          case "horizontal":
            constraint = {
              rows: constraint.rows,
              cols: constraint.cols - childNode.dimensions.cols
            };
            break;
          case "vertical":
          case "cellGroup":
            constraint = {
              rows: constraint.rows - childNode.dimensions.rows,
              cols: constraint.cols
            };
            break;
          default:
            throw new Error(`Unexpected range type`); // Unable to output type due to "never" deferred type
        }
      }
    }
    this._order = order;
    this._cache = cache;
  }
}
