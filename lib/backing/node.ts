import { Cell, GroupContent, RangeDimensions, RangeKey, Stuck } from "../ranges";
import { CellNode } from "./cell-node";
import { RangeNode } from "./range-node";

export interface RangePosition {
  col: number;

  row: number;
}

export interface CellRequestResult {
  cell: Cell | null;

  position: RangePosition;

  dimensions: RangeDimensions;

  stuck?: Stuck;
}

export function pad(
  toCol: number,
  toRow: number,
  innerDimensions: RangeDimensions,
  enclosingDimensions: RangeDimensions,
  consumer: (r: CellRequestResult) => boolean
): boolean {
  if (innerDimensions.cols < toCol) {
    const stop: boolean = consumer({
      cell: null,
      position: {
        col: innerDimensions.cols,
        row: 0
      },
      dimensions: {
        cols: enclosingDimensions.cols - innerDimensions.cols,
        rows: innerDimensions.rows
      }
    });
    if (stop) {
      return stop;
    }
  }

  if (innerDimensions.rows < toRow) {
    const stop: boolean = consumer({
      cell: null,
      position: {
        col: 0,
        row: innerDimensions.rows
      },
      dimensions: {
        cols: enclosingDimensions.cols,
        rows: enclosingDimensions.rows - innerDimensions.rows
      }
    });
    if (stop) {
      return stop;
    }
  }

  return false;
}

export function assertDimensionsFit(dimensions: RangeDimensions, constraints: RangeDimensions, key: RangeKey): void {
  if (constraints.cols < dimensions.cols || constraints.rows < dimensions.rows) {
    throw new Error(
      `Inner content dimensions (${dimensions.cols}x${dimensions.rows}) ` +
        `for range with key ${key} is greater than dimensions of parent ` +
        `(${constraints.cols}x${constraints.rows})`
    );
  }
}

export interface Node {
  readonly dimensions: RangeDimensions;

  readonly stuck?: Stuck;

  findCellsInRange(
    fromCol: number,
    toCol: number,
    fromRow: number,
    toRow: number,
    enclosingDimensions: RangeDimensions,
    consumer: (r: CellRequestResult) => boolean,
    stuck?: Stuck
  ): boolean;

  setRange(range: GroupContent, enclosingDimensions?: RangeDimensions): Node;
}

export function createNode(range: GroupContent, enclosingDimensions: RangeDimensions): Node {
  if (range.type === "cell") {
    return new CellNode(range, enclosingDimensions);
  } else {
    return new RangeNode(range, {}, enclosingDimensions);
  }
}
