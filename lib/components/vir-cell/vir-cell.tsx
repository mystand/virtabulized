import memoizeOne from "memoize-one";
import * as React from "react";
import { VirCellProps } from "./vir-cell-props";
import { virCell } from "./vir-cell.css";

export class VirCell extends React.PureComponent<VirCellProps> {
  private getStyle: (width: number, height: number, left: number, top: number) => React.CSSProperties = memoizeOne(
    (width: number, height: number, left: number, top: number): React.CSSProperties => ({
      top: `${top}px`,
      left: `${left}px`,
      width: `${width}px`,
      height: `${height}px`
    })
  );

  render(): React.ReactElement {
    const { top, left, height, width, renderer, rowSelected, colSelected }: VirCellProps = this.props;

    return (
      <div style={this.getStyle(width, height, left, top)} className={virCell}>
        {renderer.render(rowSelected, colSelected)}
      </div>
    );
  }
}
