import { CellRenderer } from "../../backing";

export interface VirCellProps {
  top: number;

  left: number;

  width: number;

  height: number;

  renderer: CellRenderer;

  rowSelected: boolean;

  colSelected: boolean;
}
