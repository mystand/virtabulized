import { CSSProperties } from "react";
import { TableModel } from "../../ranges";

export interface VirTableProps {
  model: TableModel;

  className?: string;

  style?: CSSProperties;
}
