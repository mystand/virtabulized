import memoizeOne from "memoize-one";
import * as React from "react";
import { CellDimensions, TableBacking } from "../../backing";
import { Stuck } from "../../ranges";
import { VirCell } from "../vir-cell";
import { VirTableProps } from "./vir-table-props";
import { RenderArea, VirTableState } from "./vir-table-state";
import { leftTable, topTable, virTable } from "./vir-table.css";

const MINIMUM_OVERSCAN_FACTOR: number = 0.5;
const MAXIMUM_OVERSCAN_FACTOR: number = 1.5;

function moveBound(upperBound: number, lowerBound: number, currentValue: number): number {
  if (currentValue < lowerBound) {
    return upperBound;
  } else if (currentValue > upperBound) {
    return lowerBound;
  } else {
    return currentValue;
  }
}

export class VirTable extends React.Component<VirTableProps, VirTableState> {
  private getBodyDimensions: (width: number, height: number) => React.CSSProperties = memoizeOne(
    (width: number, height: number): React.CSSProperties => ({
      height: `${height}px`,
      width: `${width}px`
    })
  );

  private scrollTopContainer: HTMLDivElement | null;
  private scrollLeftContainer: HTMLDivElement | null;

  constructor(props: VirTableProps) {
    super(props);
    this.updateArea = this.updateArea.bind(this);
    this.updateAreaOnEvent = this.updateAreaOnEvent.bind(this);
    this.updateScrollTop = this.updateScrollTop.bind(this);
    this.updateScrollLeft = this.updateScrollLeft.bind(this);
    const backing: TableBacking = TableBacking.createBacking(props.model);
    this.state = {
      backing,
      renderArea: {
        fromWidth: 0,
        toWidth: 0,
        fromHeight: 0,
        toHeight: 0
      }
    };
    this.scrollTopContainer = null;
    this.scrollLeftContainer = null;
  }

  updateScrollTop(el: HTMLDivElement): void {
    this.scrollTopContainer = el;
  }

  updateScrollLeft(el: HTMLDivElement): void {
    this.scrollLeftContainer = el;
  }

  shouldComponentUpdate(_: Readonly<VirTableProps>, nextState: Readonly<VirTableState>): boolean {
    return nextState !== this.state;
  }

  private updateArea(ref: HTMLDivElement): void {
    const { backing, renderArea }: VirTableState = this.state;

    const areaWidth: number = ref.clientWidth;
    const areaHeight: number = ref.clientHeight;
    const scrollLeft: number = ref.scrollLeft;
    const scrollTop: number = ref.scrollTop;

    if (this.scrollTopContainer) {
      this.scrollTopContainer.style.transform = `translateY(${scrollTop}px)`;
    }

    if (this.scrollLeftContainer) {
      this.scrollLeftContainer.style.transform = `translateX(${scrollLeft}px)`;
    }

    const fromWidth: number = moveBound(
      Math.max(0, scrollLeft - areaWidth * MAXIMUM_OVERSCAN_FACTOR),
      Math.max(0, scrollLeft - areaWidth * MINIMUM_OVERSCAN_FACTOR),
      renderArea.fromWidth
    );
    const toWidth: number = moveBound(
      Math.min(backing.width, scrollLeft + areaWidth + areaWidth * MINIMUM_OVERSCAN_FACTOR),
      Math.min(backing.width, scrollLeft + areaWidth + areaWidth * MAXIMUM_OVERSCAN_FACTOR),
      renderArea.toWidth
    );
    const fromHeight: number = moveBound(
      Math.max(0, scrollTop - areaHeight * MAXIMUM_OVERSCAN_FACTOR),
      Math.max(0, scrollTop - areaHeight * MINIMUM_OVERSCAN_FACTOR),
      renderArea.fromHeight
    );
    const toHeight: number = moveBound(
      Math.min(backing.height, scrollTop + areaHeight + areaHeight * MINIMUM_OVERSCAN_FACTOR),
      Math.min(backing.height, scrollTop + areaHeight + areaHeight * MAXIMUM_OVERSCAN_FACTOR),
      renderArea.toHeight
    );

    if (
      fromWidth !== renderArea.fromWidth ||
      toWidth !== renderArea.toWidth ||
      fromHeight !== renderArea.fromHeight ||
      toHeight !== renderArea.toHeight
    ) {
      this.setState({
        ...this.state,
        renderArea: { fromWidth, toWidth, fromHeight, toHeight }
      });
    }
  }

  private updateAreaOnEvent(event: React.UIEvent<HTMLDivElement>): void {
    this.updateArea(event.currentTarget);
  }

  render(): React.ReactElement {
    const { style, className }: VirTableProps = this.props;
    const { backing, renderArea }: VirTableState = this.state;

    const allCells: CellDimensions[] = backing.getCells(
      renderArea.fromWidth,
      renderArea.toWidth,
      renderArea.fromHeight,
      renderArea.toHeight
    );
    const leftCells: React.ReactElement[] = [];
    const topCells: React.ReactElement[] = [];
    const otherCells: React.ReactElement[] = [];

    for (const cell of allCells) {
      const cellRender: React.ReactElement = (
        <VirCell
          top={cell.offset.top}
          left={cell.offset.left}
          key={`${cell.col}/${cell.row}`}
          width={cell.size.width}
          height={cell.size.height}
          renderer={backing.getCellRenderer(cell.col, cell.row)}
          rowSelected={false}
          colSelected={false}
        />
      );

      if (!!cell.stuck && cell.stuck === Stuck.LEFT) {
        leftCells.push(cellRender);
      } else if (!!cell.stuck && cell.stuck === Stuck.TOP) {
        topCells.push(cellRender);
      } else {
        otherCells.push(cellRender);
      }
    }

    return (
      <div
        ref={this.updateArea}
        onScroll={this.updateAreaOnEvent}
        style={style}
        className={`${virTable} ${className || ""}`}
      >
        {!!renderArea.toWidth && (
          <div style={this.getBodyDimensions(backing.width, backing.height)}>
            {!!leftCells.length && (
              <div ref={this.updateScrollLeft} className={`${leftTable} ${className || ""}`}>
                {leftCells}
              </div>
            )}

            {!!topCells.length && (
              <div ref={this.updateScrollTop} className={`${topTable} ${className || ""}`}>
                {topCells}
              </div>
            )}

            {otherCells}
          </div>
        )}
      </div>
    );
  }

  static getDerivedStateFromProps(props: VirTableProps, state: VirTableState): VirTableState {
    const backing: TableBacking = state.backing.setModel(props.model);
    const renderArea: RenderArea = state.renderArea;

    if (backing === state.backing) {
      return state;
    }

    if (renderArea.fromHeight > backing.height) {
      renderArea.fromHeight = Math.max(0, backing.height + renderArea.fromHeight - renderArea.toHeight);
      renderArea.toHeight = backing.height;
    }

    if (renderArea.fromWidth > backing.width) {
      renderArea.fromWidth = Math.max(0, backing.width + renderArea.fromWidth - renderArea.toWidth);
      renderArea.toWidth = backing.width;
    }

    return {
      ...state,
      renderArea,
      backing
    };
  }
}
