import { TableBacking } from "../../backing";

export interface RenderArea {
  fromWidth: number;

  toWidth: number;

  fromHeight: number;

  toHeight: number;
}

export interface VirTableState {
  backing: TableBacking;

  renderArea: RenderArea;
}
