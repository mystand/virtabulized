import { ReactElement } from "react";

export interface RangeDimensions {
  readonly cols: number;

  readonly rows: number;
}

export type RangeKey = string | number;

export enum Stuck {
  TOP = "top",
  LEFT = "left"
}

export interface CellRange {
  readonly key: RangeKey;

  readonly dimensions?: RangeDimensions;

  readonly stuck?: Stuck;
}

export interface Cell extends CellRange {
  readonly type: "cell";

  readonly dimensions: RangeDimensions;

  render(columnSelected: boolean, rowSelected: boolean): ReactElement<void>;
}

export interface VerticalRange extends CellRange {
  readonly type: "vertical";

  getChildren(): RangeContent[];
}

export interface HorizontalRange extends CellRange {
  readonly type: "horizontal";

  getChildren(): RangeContent[];
}

export type RangeContent = VerticalRange | HorizontalRange | Cell;

export interface CellGroup extends CellRange {
  readonly type: "cellGroup";

  getChildren(): GroupContent[];
}

export type GroupContent = RangeContent | CellGroup;

export interface TableModel {
  readonly colWidths: number[];

  readonly rowHeights: number[];

  readonly content: GroupContent;
}
