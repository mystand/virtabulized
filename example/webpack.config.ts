import HtmlWebpackPlugin from "html-webpack-plugin";
import { join, resolve } from "path";
import { Configuration, HotModuleReplacementPlugin } from "webpack";

export default {
  target: "web",
  module: {
    rules: [
      {
        test: /.*\.tsx?$/,
        use: "ts-loader"
      },
      {
        test: /.*\.css$/,
        use: [
          "style-loader",
          {
            loader: "css-loader",
            options: {
              modules: true
            }
          }
        ]
      }
    ]
  },
  output: {
    pathinfo: true,
    path: join(__dirname, "dist"),
    publicPath: "/",
    filename: "bundle.js",
    sourceMapFilename: "bundle.map.js"
  },
  mode: "development",
  devtool: "inline-source-map",
  entry: ["./example/src/table.tsx"],
  devServer: {
    host: "0.0.0.0",
    port: 1337,
    hot: true,
    quiet: false,
    historyApiFallback: true,
    contentBase: resolve(__dirname, "src"),
    publicPath: "/",
    disableHostCheck: true
  },
  plugins: [
    new HotModuleReplacementPlugin(),
    new HtmlWebpackPlugin({
      template: "example/public/index.template.ejs"
    })
  ],
  resolve: {
    extensions: [".ts", ".tsx", ".js", ".css"]
  }
} as Configuration;
