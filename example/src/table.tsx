import * as React from "react";
import { render } from "react-dom";
import { RangeContent, Stuck, TableModel, VirTable } from "../../lib";

const simpleModel: TableModel = {
  colWidths: new Array(1000).fill(null).map(() => 100),
  rowHeights: new Array(1000).fill(null).map(() => 100),
  content: {
    type: "vertical",
    key: "root",
    getChildren(): RangeContent[] {
      return new Array(1000).fill(null).map((_, i) => ({
        key: `${i}`,
        type: "horizontal",
        stuck: i === 0 ? Stuck.TOP : undefined,
        getChildren(): RangeContent[] {
          return new Array(1000).fill(null).map((__, j) => ({
            key: `${j}`,
            type: "cell",
            stuck: j === 0 ? Stuck.LEFT : undefined,
            dimensions: { cols: 1, rows: 1 },
            render(): React.ReactElement<void> {
              if (i === 0 && j === 0) {
                return <>Empty</>;
              }

              return (
                <>
                  {i}/{j}
                </>
              );
            }
          }));
        }
      }));
    }
  }
};

render(<VirTable model={simpleModel} style={{ height: "100%", width: "100%" }} />, document.getElementById("root"));
