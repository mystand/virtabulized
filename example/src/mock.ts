export enum ApartmentType {
  K1 = "1K",
  K2 = "2K",
  K3 = "3K",
  K4 = "4K",
  K5 = "5K",
  C0 = "0C",
  C1 = "1C",
  C2 = "2C",
  C3 = "3C",
  C4 = "4C",
  C5 = "5C",
  D = "D"
}

export interface SalesFigures {
  count: number;

  area: number;

  price: number;
}

export type PlannedSales = Record<ApartmentType, SalesFigures> & {
  month: Date;
};

export interface ConstructionObject {
  gp: string;

  plannedSales: PlannedSales[];
}

export interface Project {
  title: string;

  objects: ConstructionObject[];
}

export interface Contour {
  title: string;

  projects: Project[];
}

const latin: string = "abcdefghijklmnopqrstuvwxyz";

function code(numChars: number): string {
  return new Array(numChars)
    .fill(0)
    .map(() => Math.round(Math.random() * latin.length))
    .map((i: number) => latin.charAt(i))
    .join("");
}

function sales(): Record<ApartmentType, SalesFigures> {
  return Object.keys(ApartmentType)
    .map((k) => ApartmentType[k])
    .reduce(
      (acc: PlannedSales, k: ApartmentType): PlannedSales => ({
        ...acc,
        [k]: {
          count: Math.random() * 10,
          area: Math.random() * 1000,
          price: Math.random() * 100000000
        }
      }),
      {}
    );
}

export const data: Contour[] = new Array(5).fill(null).map(() => ({
  title: code(3),
  projects: new Array(10).fill(null).map(
    (): Project => ({
      title: code(3).toUpperCase(),
      objects: new Array(8).fill(null).map(
        (_, i): ConstructionObject => ({
          gp: `${i}`,
          plannedSales: new Array(240).fill(null).map((__, j) => ({
            month: new Date(2010 + j / 12, (j % 12) + 1),
            ...sales()
          }))
        })
      )
    })
  )
}));
