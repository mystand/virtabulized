import commonjs from "rollup-plugin-commonjs";
import postcss from "rollup-plugin-postcss";
import typescript from "rollup-plugin-typescript2";

export default {

  input: "lib/index.ts",

  output: {
    file: "dist/index.js",
    format: "umd",
    globals: {
      "react": "React",
      "react-dom": "react-dom"
    },
    name: "virtabulized",
    sourcemap: true
  },

  plugins: [
    typescript(),
    commonjs(),
    postcss({
      modules: true,
      namedExports: true
    })
  ],

  external: [ "react", "react-dom" ]

};
